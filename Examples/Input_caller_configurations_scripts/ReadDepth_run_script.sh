#!/bin/bash
#
# title		ReadDepth_run_script.sh
# description	Shell script for running ReadDepth
# author		s.friedrich
# date		2020-02-29
# version		0.1    
# usage		bash ReadDepth_run_script.sh [Arguments] or -help
# notes		requires R and Samtools; entrypoints, gcWinds and Mapability files according to https://github.com/chrisamiller/readDepth 

####### Arguments ##############################################################################################################################

# Command line help #
Help() {
    echo "Usage: $0 [  ReadDepth-path-to  Alignment-file ]" >&2
    echo
    echo " Parameters:"
    echo "  1	ReadDepth-path-to		Path to ReadDepth directory (strucure as described on https://github.com/chrisamiller/readDepth "
    echo "  2	Alignment-file       		Alignment file in *.bam format"
    echo
    echo "* Samtools and R must be installed"	
    echo "ReadDepth parameters setting in readDepth/params file (see example); entrypoints, gcWinds and Mapability files according to https://github.com/chrisamiller/readDepth "	
    exit 1
}

# Check parameters #
if [ "$#" -eq 0 ]
	then
	Help
fi

if [ "$#" -eq 1 ]
	then
		while getopts ":h" option; do
			case $option in
				h) # display Help
        				Help
        				 exit;;
				\?) # incorrect option
			        	echo "Error: Invalid option"
					exit;;
				*) # missing arguments
					echo "Arguments missing"
					Help
					exit ;;
			esac
	done
fi

# Script arguments #
ARGS=2

# Path to alignment #
FILE_IN=$2
BAM=$(basename "${FILE_IN}")
BAM2=$(echo "${BAM%.*}")
PATH_IN=$(dirname "${FILE_IN}")

# Path to ReadDepth folder #
READDEPTH=$1
mkdir -p $READDEPTH/reads/${BAM2}_tmp
mkdir -p $READDEPTH/output/${BAM2}

# Echo parameters #
echo "ReadDepth folder	$(cd "$(dirname "$READDEPTH")"; pwd)/$(basename "$READDEPTH")"
echo "Alignment file 		$(cd "$(dirname "$PATH_IN/$BAM")"; pwd)/$(basename "$PATH_IN/$BAM")"

# ReadDepth pre-processing ###############################################################################################

# *.bam to *.bed conversion
#samtools view -F 4 $PATH_IN/$BAM | awk 'OFS="\t"{print $3,$4-1,$4}' > $READDEPTH/reads/${BAM2}_tmp/$BAM2.bed

# In case 'chr' in first column is missing 
#LINE=$(head -n 1 $READDEPTH/reads/${BAM2}_tmp/$BAM2.bed)
#if [[ $LINE = *chr* ]] ||  [[ $LINE = *CHR* ]]
#	then
#		mv $READDEPTH/reads/${BAM2}_tmp/${BAM2}.bed $READDEPTH/reads/${BAM2}_tmp/${BAM2}_step2.bed

#	else
#		sed -e 's/^/chr/' $READDEPTH/reads/${BAM2}_tmp/$BAM2.bed > $READDEPTH/reads/${BAM2}_tmp/${BAM2}_step2.bed
#fi

# Split *.bed into file per chromosome
#cd $READDEPTH/reads/${BAM2}_tmp/
#awk '{print $0 >> $1".bed"}' $READDEPTH/reads/${BAM2}_tmp/${BAM2}_step2.bed 

# Create readinfo file
#for i in chr* ; do wc -l $i >> readinfo_tmp.txt ; done
#sed 's; ;\treads/;' readinfo_tmp.txt > readinfo.txt 
#echo "#Alignment file: $(cd "$(dirname "$PATH_IN/$BAM")"; pwd)/$(basename "$PATH_IN/$BAM")" >> readinfo.txt

# Copy chr*.bed files to folder 'reads'
#cp chr* $READDEPTH/reads/ && cp readinfo.txt $READDEPTH/reads/

# Run ReadDepth ########################################################################################################


#Rscript ReadDepth.r $READDEPTH --save >> $READDEPTH/output/readdepth_log.txt

# Save output files in separate folder ############################################################################################
cd $READDEPTH/output
tmp=$(ls -tF | grep -v / | head -9)
cd $READDEPTH/output/$BAM2
for file in *; do mv "$file" "${BAM2}_readdepth_${file}" ; done 

