#!/bin/bash
#
# title		CNVnator_run_script.sh
# description	Shell script for running CNVnator
# author		s.friedrich
# date		2020-02-29
# version		0.1    
# usage		bash CNVnator_run_script.sh [Arguments] or -help
# notes		requires CNVnator installed and a reference genome split by chromosome

####### Arguments ##############################################################################################################################

# Command line help #
Help() {
    echo "Usage: $0 [  CNVnator-path-to   CNVnator-bin-size   Reference-genome-file   Reference-per-chromosome-path   Alignment-file  ]" >&2
    echo
    echo " Arguments:"
    echo "  1	CNVnator-path-to			Path to program 'cnvnator' "
    echo "  2	CNVnator-bin-size			Window size to calculate CNVs [bp], e.g. 400 "
    echo "  3	Reference-genome-file			Reference genome file in *.fa format"
    echo "  4	Reference-per-chromosome-path		Path to reference genome split per chromosome"
    echo "  5	Alignment-file       			Alignment file in *.bam format"
    exit 1
}

# Check parameters #
if [ "$#" -eq 0 ]
	then
	Help
fi

if [ "$#" -eq 1 ]
	then
		while getopts ":h" option; do
			case $option in
				h) # display Help
        				Help
        				 exit;;
				\?) # incorrect option
			        	echo "Error: Invalid option"
					exit;;
				*) # missing arguments
					echo "Arguments missing"
					Help
					exit ;;
			esac
	done
fi

# Script arguments #
ARGS=5        

# Path to reference genome #
REF=$3 
FASTAPERCHR=$4 

# Path to alignment & CNVnator output #
FILE_IN=$5
BAM=$(basename "${FILE_IN}")
PATH_IN=$(dirname "${FILE_IN}")
PATH_OUT="$PATH_IN/CNVnator_out"
mkdir -p $PATH_OUT

# Path to CNVnator & parameters #
CNVNATOR=$1
BIN=$2
ROOT="${BAM%.bam}_w${BIN}.root"

# Echo parameters #
echo "CNVnator path			$(cd "$(dirname "$CNVNATOR")"; pwd)/$(basename "$CNVNATOR")"
echo "CNVnator window size (bin)	$BIN"
echo "CNVnator path to outfile 	$(cd "$(dirname "$PATH_OUT")"; pwd)/$(basename "$PATH_OUT")"
echo "Reference genome (*.fa) 	$(cd "$(dirname "$REF")"; pwd)/$(basename "$REF")"
echo "Reference genome per chromosome path $(cd "$(dirname "$FASTAPERCHR")"; pwd)/$(basename "$FASTAPERCHR")"
echo "Alignment file 			$(cd "$(dirname "$PATH_IN/$BAM")"; pwd)/$(basename "$PATH_IN/$BAM")"

####### Run CNVnator ################################################################################################################################
cd $CNVNATOR

# 1. Creating root file
./cnvnator -root $PATH_OUT/$ROOT -genome $REF -tree $PATH_IN/$BAM
echo "step 1/5 done"

# 2. Making histogram
./cnvnator -d $FASTAPERCHR -root $PATH_OUT/$ROOT -genome $REF -tree $PATH_IN/$BAM -his $BIN
echo "step 2/5 done"

# 3. Calculating statistics
./cnvnator -d $FASTAPERCHR -root $PATH_OUT/$ROOT -genome $REF -tree $PATH_IN/$BAM -stat $BIN
echo "step 3/5 done"

# 4. Partitioning
./cnvnator -d $FASTAPERCHR -root $PATH_OUT/$ROOT -genome $REF -tree $PATH_IN/$BAM -partition $BIN
echo "step 4/5 done"

# 5. Identifying CNVs
./cnvnator -d $FASTAPERCHR -root $PATH_OUT/$ROOT -genome $REF -tree $BAM -call $BIN > $PATH_OUT/${ROOT%.root}.csv
echo "step 5/5 done .... CNVnator complete"
