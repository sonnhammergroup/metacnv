#!/usr/bin/Rscript
# title		ReadDepth.r
# description	R script for running ReadDepth
# author		s.friedrich
# date		2020-02-29
# version		0.1    
# usage		bash ReadDepth_run_script.sh [Arguments] or -help
# notes		Entrypoints, gcWinds and Mapability files according to https://github.com/chrisamiller/readDepth 


# Change to ReadDepth directory ####################################################################################################
args <- commandArgs() 
#print(args)
setwd(args[6])
getwd()

# Run ReadDepth ##################################################################################################################
library("readDepth")
rdo = new("rdObject")
rdo = readDepth(rdo)
rdo.mapCor = rd.mapCorrect(rdo, minMapability=0.75)
rdo.mapCor.gcCor = rd.gcCorrect(rdo.mapCor)
segs = rd.cnSegments(rdo.mapCor.gcCor)
writeSegs(segs)
writeAlts(segs,rdo)
writeThresholds(rdo)
save.image("output/mysave.Rdata")
