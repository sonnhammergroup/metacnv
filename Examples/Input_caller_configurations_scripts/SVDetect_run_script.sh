#!/bin/bash
#
# title		SVDetect_run_script
# description	Shell script for running SVDetect
# author		s.friedrich
# date		2020-02-29
# version		0.1    
# usage		bash SVDetect_run_script [Arguments] or -help
# notes		requires SVDetect installed and the simulatedNULLalignemnt.bam (see Example/Input_caller_configuration

####### Arguments ##############################################################################################################################

# Command line help #
Help() {
    echo "Usage: $0 [  SVDetect-path-to   Configuration-file   Alignment-file ]" >&2
    echo
    echo " Parameters:"
    echo "  1	SVDetect-path-to		Path to SVDetect containing bin/SVDetect and scripts/BAM_preprocessingPairs.pl"
    echo "  2	Configuration-file       	Configuration file for SVDetect; example in Example/Input_caller_configurations/"
    echo "  3	Alignment-file       	Alignment file in *.bam format"
    echo
    echo "* SVDetect must be installed; path to outfiles configured in configuration file"	
    exit 1
}

# Check parameters #
if [ "$#" -eq 0 ]
	then
	Help
fi

if [ "$#" -eq 1 ]
	then
		while getopts ":h" option; do
			case $option in
				h) # display Help
        				Help
        				 exit;;
				\?) # incorrect option
			        	echo "Error: Invalid option"
					exit;;
				*) # missing arguments
					echo "Arguments missing"
					Help
					exit ;;
			esac
	done
fi

# Script arguments #
ARGS=3

# Path to alignment #
FILE_IN=$3
BAM=$(basename "${FILE_IN}")
BAM2=$(echo "${BAM%.*}")
PATH_IN=$(dirname "${FILE_IN}")
PATH_OUT="$PATH_IN/svdetect_output"
mkdir -p $PATH_OUT

# Path to SVDetect #
SVDETECT=$1

# Path to SVDetect configuration file #
CONF=$2

# Echo parameters #
echo "SVDetect path 		$(cd "$(dirname "$SVDETECT")"; pwd)/$(basename "$SVDETECT")"
echo "Configuration file 	$(cd "$(dirname "$CONF")"; pwd)/$(basename "$CONF")"
echo "Alignment file 		$(cd "$(dirname "$PATH_IN/$BAM")"; pwd)/$(basename "$PATH_IN/$BAM")"

# SVDetect pre-processing ##########################################################################################
# Create Checked bam file (tumor and normal separately because it will take a while)
cd $SVDETECT
perl scripts/BAM_preprocessingPairs.pl -t 1 -p 1 -d -o $PATH_OUT $PATH_IN/$BAM

# SVDetect run ####################################################################################################
# Apply SVdetect on checked bam files (see configuration example)
bin/SVDetect cnv -conf $CONF


