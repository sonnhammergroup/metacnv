#include "../compress/compress_XY.h"
#include "../match_to_ref/match_to_ref.h"

extern float a_threshold_XY;
extern float d_threshold_XY;

extern float coverageMetaCNV;
extern float coverageMetaCNV_XY;

void compress_XY(std::vector<cnvFrame> const &cnvXY){
	std::string filepath = "../Output/MetaCNV_segments_" + ::filename;
	std::fstream fout_XY(boost::lexical_cast<std::string>(filepath), fout_XY.out| fout_XY.app);  // std::ios_base::app
    
    std::string filepath_p = "../Output/MetaCNV_parameters_" + ::filename;
	std::fstream fout_XY_p(boost::lexical_cast<std::string>(filepath_p), fout_XY_p.out| fout_XY_p.app);
    
    std::string rdArray ;
    std::string rdValueAsString;
    std::string svArray ;
    std::string svValueAsString;
    std::string svEfactorArray ;
    std::string svEfactorAsString;
    std::string cntypeArray ;
    std::string commentArray ;
    double weightedErrorScore;
    
    cnvFrame buffer("", 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, "", 0.0, ""); // making all the members NULL
	fout_XY << std::fixed << std::setprecision(1);
    
 	//fout_XY << "chr\t" << "segment.start\t" << "segment.end\t" << "segment.length\t" << "MetaCNV.cn\t" << "MetaCNV.errorScore\t" << "MetaCNV.comments\t" <<  "RD.cn.array\t" << "SV.cn.array\t" << "CNVnator.type.array" << std::endl;
    fout_XY_p <<"MetaCNV coverage II\t" << ( ( coverageMetaCNV_XY / 2132683.10 )  ) << " % of GRCh38.84 allosomes" << std::endl;

	for(int i = 0; i < cnvXY.size(); ++i){
		if (buffer.chr.empty()){
			buffer.chr = cnvXY[i].chr;
			buffer._start = cnvXY[i]._start;
			buffer._end = cnvXY[i]._end;
			buffer.value = cnvXY[i].value;
			//buffer.rdValue = cnvXY[i].rdValue;
			//buffer.svValue = cnvXY[i].svValue;
            //buffer.rdBias = cnvXY[i].rdBias;
            buffer.cnType = cnvXY[i].cnType;
            buffer.errorScore = cnvXY[i].errorScore * (buffer._end - buffer._start) ;
            weightedErrorScore = buffer.errorScore;
			buffer.comment = cnvXY[i].comment;
            rdValueAsString = std::to_string(cnvXY[i].rdValue).substr(0, 4); 
            //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
            rdArray = rdValueAsString;
            svValueAsString = std::to_string(cnvXY[i].svValue).substr(0, 4);
            //replace(svValueAsString.begin(), svArray.end(), ',', '.');
            svArray = svValueAsString;
            //svEfactorAsString = std::to_string(cnv[i].svEfactor).substr(0, 3);
            //svEfactorArray = svEfactorAsString;
            cntypeArray = buffer.cnType;
            commentArray = buffer.comment;
		}
		else{
			if (buffer.chr == cnvXY[i].chr && fabs(buffer.value - cnvXY[i].value ) < 0.05 ) {
				if ((cnvXY[i]._end % 10) == 0){
                    buffer._end = cnvXY[i]._end; 
                }
                else 
                    buffer._end = cnvXY[i]._end - 1 ;
                    
                rdValueAsString = std::to_string(cnvXY[i].rdValue).substr(0, 4); 
                //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
                svValueAsString = std::to_string(cnvXY[i].svValue).substr(0, 4);
                //replace(svValueAsString.begin(), svArray.end(), ',', '.');
                
                //buffer.rdBias = cnvXY[i].rdBias;
                
                // weghted error score
                buffer.errorScore = cnvXY[i].errorScore * (buffer._end - buffer._start) ;
                weightedErrorScore = weightedErrorScore + buffer.errorScore  ;
                
                // combine all RD values
                if (rdArray.find(rdValueAsString) == std::string::npos){
                    rdArray.append( "; " );
					rdArray.append(rdValueAsString);
				}
                
                // combine all SV values
                if (svArray.find(svValueAsString) == std::string::npos){
                    svArray.append( "; " );
					svArray.append(svValueAsString);
				}

                // combine all SV Efactor values
                if (svEfactorArray.find(svEfactorAsString) == std::string::npos){
                    svEfactorArray.append( "; " );
					svEfactorArray.append(svEfactorAsString);
				}                
                // combine all CNVnator types
                if (cntypeArray.find(cnvXY[i].cnType) == std::string::npos){
					cntypeArray.append( "; " );
					cntypeArray.append(cnvXY[i].cnType);
				}
                
                // combine all comments
				if (commentArray.find(cnvXY[i].comment) == std::string::npos){
					commentArray.append( "; " );
					commentArray.append(cnvXY[i].comment);
				}
			}
			else{
                fout_XY << buffer.chr << "\t" << buffer._start << "\t" << buffer._end << "\t" << buffer._end - buffer._start << "\t" << buffer.value << "\t" << weightedErrorScore / (buffer._end - buffer._start) << "\t" << commentArray << "\t" << rdArray << "\t" << svArray << "\t" << cntypeArray << std::endl ;
				buffer.chr = cnvXY[i].chr;
				buffer._start = cnvXY[i]._start;
				buffer._end = cnvXY[i]._end;
				buffer.value = cnvXY[i].value;
				//buffer.rdValue = cnv[i].rdValue;
				//buffer.svValue = cnv[i].svValue;
                //buffer.rdBias = cnvXY[i].rdBias;
                buffer.cnType = cnvXY[i].cnType;
                buffer.errorScore = cnvXY[i].errorScore;
                weightedErrorScore = buffer.errorScore * (buffer._end - buffer._start);
				buffer.comment = cnvXY[i].comment;
                rdValueAsString = std::to_string(cnvXY[i].rdValue).substr(0, 4); 
                rdArray = rdValueAsString;
                //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
                svValueAsString = std::to_string(cnvXY[i].svValue).substr(0, 4);
                svArray = svValueAsString;
                //replace(svValueAsString.begin(), svArray.end(), ',', '.');
                //svEfactorAsString = std::to_string(cnv[i].svEfactor).substr(0, 3);
                //svEfactorArray = svEfactorAsString;
                cntypeArray = buffer.cnType;
                commentArray = buffer.comment;
			}
		}
	}

    if (::matchGenes == "Yes"){
		std::cout << "\n- Mapping MetaCNV output to the reference genome on gene level " << std::endl;
        std::string match_file_in = "../Input/Homo_sapiens.GRCh38.84_onlyGenes.gtf";
        std::string filepath_tmp = filepath;
        std::string match_file_out = filepath_tmp.erase(filepath_tmp.find_last_of("."), std::string::npos) + "_mappedToRef.genes.dat" ;//."./Output/MetaCNV_segments_mappedtoRef.genes_" + ::filename;
        match_to_ref(filepath, match_file_in, match_file_out);
	}

	if (::matchExons == "Yes"){
		std::cout << "\n- Mapping MetaCNV output to the reference genome on exon level " << std::endl;
        std::string match_file_in = "../Input/Homo_sapiens.GRCh38.84_onlyExons.gtf" ;
        std::string filepath_tmp = filepath;
        std::string match_file_out = filepath_tmp.erase(filepath_tmp.find_last_of("."), std::string::npos) + "_mappedToRef.exons.dat" ;//."./Output/MetaCNV_segments_mappedtoRef.genes_" + ::filename;
        match_to_ref(filepath, match_file_in, match_file_out);
	}

    std::cout << "\n\n                             *** Done ***" << std::endl;
    std::cout << "---------------------------------------------------------------------\n\n" << std::endl;
    }
