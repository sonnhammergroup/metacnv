#include "../compress/compressF.h"
#include "../match_to_ref/match_to_ref.h"

extern float a_threshold;
extern float d_threshold;
extern float coverageRD;
extern float coverageSV;
extern float coverageCNVnator;
extern float coverageMetaCNV;

void compressF(std::vector<cnvFrame> const &cnv){
	std::string filepath = "../Output/MetaCNV_segments_" + ::filename;
	std::ofstream fout(boost::lexical_cast<std::string>(filepath));
        
    std::string filepath_p = "../Output/MetaCNV_parameters_" + ::filename;
	std::ofstream fout_p(boost::lexical_cast<std::string>(filepath_p));

    std::string rdArray ;
    std::string rdValueAsString;
    std::string svArray ;
    std::string svValueAsString;
    std::string svEfactorArray ;
    std::string svEfactorAsString;
    std::string cntypeArray ;
    std::string commentArray ;
    double weightedErrorScore;
    int counter;
	cnvFrame buffer("", 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, "", 0.0, ""); // making all the members NULL 
    fout << std::fixed << std::setprecision(1);
    
    fout_p << "*****************************************\n******* MetaCNV 1.3 parameters *******\n*****************************************"
    << "\n\nGender\t\t\t"<< ::gender 
    << "\n\nReadDepth infile\t\t" << ::readDepth_filepath << "\nReadDepth bias\t\t"  << cnv[10].rdBias << "\nReadDepth coverage\t" << ( coverageRD / 3088286.4 ) << "% of GRCh38.84" 
    << "\n\nSVDetect infile\t\t\t" << ::svdetect_filepath << "\nSVDetect coverage\t\t" << ( coverageSV / 3088286.4 ) << "% of GRCh38.84" 
    << "\n\nCNVnator infile\t\t" << ::cnvNator_filepath << "\nCNVnator coverage\t\t" << ( coverageCNVnator / 30882864 ) << " % of GRCh38.84" 
    << "\n\nMetaCNV outfile\t\t" << ::filename << "\nMetaCNV coverage\t" << (std::min( ( coverageMetaCNV / 30882864.0 ), 100.0 ) ) << "% of GRCh38.84" 
    << "\n\nThreshold for deletions\t" <<  d_threshold << "\nThreshold for amplific.\t" <<  a_threshold 
    << std::endl;
       
	fout << "chr\t" << "segment.start\t" << "segment.end\t" << "segment.length\t" << "MetaCNV.cn\t" << "MetaCNV.errorScore\t" << "MetaCNV.comments\t"  << "RD.cn.array\t" << "SV.cn.array\t" << "CNVnator.type.array" << std::endl;
	
	for (int i = 0; i < cnv.size(); ++i){
		if (buffer.chr.empty() ) {
            buffer.chr = cnv[i].chr;
			buffer._start = cnv[i]._start;
			buffer._end = cnv[i]._end;
			buffer.value = cnv[i].value;
			//buffer.rdValue = cnv[i].rdValue;
			//buffer.svValue = cnv[i].svValue;
            buffer.cnType = cnv[i].cnType;
            buffer.errorScore = cnv[i].errorScore * (buffer._end - buffer._start) ;
            weightedErrorScore = buffer.errorScore;
			buffer.comment = cnv[i].comment;
            rdValueAsString = std::to_string(cnv[i].rdValue).substr(0, 4); 
            //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
            rdArray = rdValueAsString;
            svValueAsString = std::to_string(cnv[i].svValue).substr(0, 4);
            //replace(svValueAsString.begin(), svArray.end(), ',', '.');
            svArray = svValueAsString;
            //svEfactorAsString = std::to_string(cnv[i].svEfactor).substr(0, 3);
            //svEfactorArray = svEfactorAsString;
            cntypeArray = buffer.cnType;
            commentArray = buffer.comment;
		}
		else{
			if (buffer.chr == cnv[i].chr && fabs(buffer.value - cnv[i].value) < 0.05 ) {
                if ((cnv[i]._end % 10) == 0){
                    buffer._end = cnv[i]._end; 
                }
                else 
                    buffer._end = cnv[i]._end - 1;
                    
                rdValueAsString = std::to_string(cnv[i].rdValue).substr(0, 4); 
                //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
                svValueAsString = std::to_string(cnv[i].svValue).substr(0, 4);
                //replace(svValueAsString.begin(), svArray.end(), ',', '.');
                
                // weghted error score
                buffer.errorScore = cnv[i].errorScore * (buffer._end - buffer._start) ;
                if (counter = 1) {
                    weightedErrorScore = buffer.errorScore ;
                }
                else {
                    weightedErrorScore = weightedErrorScore + buffer.errorScore  ;
                }
                
                // combine all RD values
                if (rdArray.find(rdValueAsString) == std::string::npos){
                    rdArray.append( "; " );
					rdArray.append(rdValueAsString);
				}
                
                // combine all SV values
                if (svArray.find(svValueAsString) == std::string::npos){
                    svArray.append( "; " );
					svArray.append(svValueAsString);
				}

                // combine all SV Efactor values
                /*if (svEfactorArray.find(svEfactorAsString) == std::string::npos){
                    svEfactorArray.append( "; " );
					svEfactorArray.append(svEfactorAsString);
				} */ 
   
                // combine all CNVnator types
                if (cntypeArray.find(cnv[i].cnType) == std::string::npos){
					cntypeArray.append( "; " );
					cntypeArray.append(cnv[i].cnType);
				}
                
                // combine all comments
				if (commentArray.find(cnv[i].comment) == std::string::npos){
					commentArray.append( "; " );
					commentArray.append(cnv[i].comment);
				}
                counter ++;
			}
			else{
 				fout << buffer.chr << "\t" << buffer._start << "\t" << buffer._end << "\t" << buffer._end - buffer._start << "\t" << buffer.value << "\t" << weightedErrorScore / (buffer._end - buffer._start) << "\t" << commentArray << "\t" << rdArray << "\t" << svArray << "\t" << cntypeArray <<  std::endl;
				buffer.chr = cnv[i].chr;
				buffer._start = cnv[i]._start;
				buffer._end = cnv[i]._end;
				buffer.value = cnv[i].value;
				//buffer.rdValue = cnv[i].rdValue;
				//buffer.svValue = cnv[i].svValue;
                buffer.cnType = cnv[i].cnType;
                buffer.errorScore = cnv[i].errorScore;
                weightedErrorScore = buffer.errorScore * (buffer._end - buffer._start);
				buffer.comment = cnv[i].comment;
                rdValueAsString = std::to_string(cnv[i].rdValue).substr(0, 4); 
                rdArray = rdValueAsString;
                //replace(rdValueAsString.begin(), rdArray.end(), ',', '.');
                svValueAsString = std::to_string(cnv[i].svValue).substr(0, 4);
                svArray = svValueAsString;
                //replace(svValueAsString.begin(), svArray.end(), ',', '.');
                //svEfactorAsString = std::to_string(cnv[i].svEfactor).substr(0, 3);
                //svEfactorArray = svEfactorAsString;
                cntypeArray = buffer.cnType;
                commentArray = buffer.comment;
                counter = 1;
			}
		}
	}

    if (::matchGenes == "Yes"){
 		std::cout << "\n- Mapping MetaCNV output to the reference genome on gene level " << std::endl;
        std::string match_file_in = "../Input/Homo_sapiens.GRCh38.84_onlyGenes.gtf";
        std::string filepath_tmp = filepath;
        std::string match_file_out = filepath_tmp.erase(filepath_tmp.find_last_of("."), std::string::npos) + "_mappedToRef.genes.dat" ;//."./Output/MetaCNV_segments_mappedtoRef.genes_" + ::filename;
        match_to_ref(filepath, match_file_in, match_file_out);
	}

	if (::matchExons == "Yes"){
        std::cout << "\n- Mapping MetaCNV output to the reference genome on exon level " << std::endl;
        std::string match_file_in = "../Input/Homo_sapiens.GRCh38.84_onlyExons.gtf" ;
        std::string filepath_tmp = filepath;
        std::string match_file_out = filepath_tmp.erase(filepath_tmp.find_last_of("."), std::string::npos) + "_mappedToRef.exons.dat" ;//."./Output/MetaCNV_segments_mappedtoRef.genes_" + ::filename;
        match_to_ref(filepath, match_file_in, match_file_out);
	}
    
    std::cout << "\n\n                             *** Done ***" << std::endl;
    std::cout << "---------------------------------------------------------------------\n\n" << std::endl;
}
