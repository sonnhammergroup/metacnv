#ifndef _STRUCTURES_H_
#define _STRUCTURES_H_

#include "libraries.h"

struct cnvSegs{
	std::string chr;
	int _start;
	int _end;
	float value;
};

struct NewSegmentFrame{
	std::string chr;
	int _start;
	int _end;
	float rdValue;
    float svValue;
    float rdBias;
    float svEfactor;
};

struct cnvFrame{
	std::string chr;
	int _start;
	int _end;
	float value;
	float rdValue;
	float svValue;
    float rdBias;
    float svEfactor;
    std::string cnType;
    float errorScore;
    std::string comment;
    
	cnvFrame( std::string chr, int _start, int _end, float value, float rdValue, float svValue, float rdBias, float svEfactor, std::string cnType, float errorScore, std::string comment) : 
	chr(chr), _start(_start), _end(_end), value(value), rdValue(rdValue), svValue(svValue), rdBias(rdBias), svEfactor(svEfactor), cnType(cnType), errorScore(errorScore), comment(comment) {}
};

struct exonFrame{
	std::string chr;
	std::string gid; //Gene ID
	std::string biotype;
	std::string name;
	std::string strand;
	std::string eid; //Exon ID
	int _start; // Exon start
	int _end; // Exon end
};

struct geneFrame{
	std::string chr;
	std::string gid; //Gene ID
	std::string biotype;
	std::string name;
	std::string strand;
	int _start; // Gene start
	int _end; // Gene end
};

struct cnvNatorFrame{
	std::string mutation;
	std::string chr;
	int _start;
	int _end;
};

extern std::string gender;
extern std::string match;
extern std::string matchGenes;
extern std::string matchExons;
extern std::string match_file_in;
extern std::string match_file_out;
extern std::string filename;
extern std::string filenameXY;
extern std::string readDepth_filepath;
extern std::string svdetect_filepath;
extern std::string cnvNator_option;
extern std::string cnvNator_filepath;

extern float d_threshold;
extern float a_threshold;
extern float d_threshold_XY;
extern float a_threshold_XY;

extern float coverageRD;
extern float coverageSV;
extern float coverageCNVnator;
extern float coverageMetaCNV;
extern float coverageMetaCNV_XY;

#endif