#include "../new_segments/new_segments.h"
#include "../combine/crunch.h"
#include "../normalize/normalize.h"
#include "../normalize/normalize_XY.h"

//sorting input data by chromosome and start position
bool compareData(const cnvSegs& i, const cnvSegs& j){
    if (i.chr < j.chr)
        return true;
    else if (i.chr > j.chr)
        return false;

    if (i._start < j._start)
        return true;
    else if (i._start > j._start)
        return false;

    return false;
}

bool newSegments(std::vector <cnvNatorFrame> &cnvNator, std::vector <cnvNatorFrame> &cnvNatorXY){
	
	std::ifstream readDepth;
	std::ifstream svdetect;
	
	readDepth.open(::readDepth_filepath.c_str());
	if (readDepth.fail()) {
		std::cout << "Error opening ReadDepth file (file does not exist) ..." << std::endl;
		return EXIT_FAILURE;
	}
	
	
	svdetect.open(::svdetect_filepath.c_str());
	if (svdetect.fail()) {
		std::cout << "Error opening SVDetect file (file does not exist) ..." << std::endl;
		return EXIT_FAILURE;
	}
	
	std::vector <cnvSegs> rd; // readDepth vector
	std::vector <cnvSegs> sv; // SVdetect vector
	
	cnvSegs buffer;
	
	std::vector <NewSegmentFrame> segment; // vector for new segments
	std::vector <NewSegmentFrame> segmentXY; // vector for new segments allosomes
	
	NewSegmentFrame segBuffer;
	
	std::vector <std::string> lineSplit;
	std::string line;
	std::stringstream nr_decimals;
    std::stringstream tmp2;

	nr_decimals.precision(4);

	/* Reading input files from RreadDepth and SVDetect */
    std::cout << "MetaCNV has started:" << std::endl;
	std::cout << "- step 1/5 Reading files" << std::endl;
    coverageRD = 0;
	while(std::getline(readDepth, line)){
		boost::split(lineSplit, line, boost::is_any_of("\t"));

		if (lineSplit[0] != "chr" && lineSplit[0] != "Chr" && lineSplit[0] != "CHR" && lineSplit[0] != "chromosome" && lineSplit[0] != "Chromosome"){
			if (lineSplit[0].size() > 2){
				lineSplit[0].erase(lineSplit[0].begin(),lineSplit[0].begin()+3); //  "chr" as prefix will be erased 
			}
			buffer.chr = lineSplit[0];
            
			buffer._start = boost::lexical_cast<int>(lineSplit[1]);
            buffer._end = boost::lexical_cast<int>(lineSplit[2]);
            
			nr_decimals << std::fixed << boost::lexical_cast<float>(lineSplit[4]);
			nr_decimals >> buffer.value;
			nr_decimals.clear();
			rd.push_back(buffer);
            coverageRD = (coverageRD *1.0 ) + ( (buffer._end - buffer._start) *1.0 /10 ) ; // calculate coverage (length divided by 10 since biggest possible int value is 2 billion)
		}
		lineSplit.clear();
	}
	
	while(std::getline(svdetect, line)){
		boost::split(lineSplit, line, boost::is_any_of("\t"));
		
		if (lineSplit[0] != "chr" && lineSplit[0] != "Chr" && lineSplit[0] != "CHR" && lineSplit[0] != "chromosome" && lineSplit[0] != "Chromosome"){
				
			if (lineSplit[0].size() > 2){
				
				lineSplit[0].erase(lineSplit[0].begin(),lineSplit[0].begin()+3);
			}
			buffer.chr = lineSplit[0];
			buffer._start = boost::lexical_cast<int>(lineSplit[1]);
			buffer._end = boost::lexical_cast<int>(lineSplit[2]);
			nr_decimals << std::fixed << boost::lexical_cast<float>(lineSplit[5]);
			nr_decimals >> buffer.value;
			nr_decimals.clear();
			sv.push_back(buffer);
            coverageSV = (coverageSV *1.0 ) + ( (buffer._end - buffer._start) *1.0 /10 ) ; // calculate coverage (length divided by 10 since biggest possible int value is 2 billion)
		}
		lineSplit.clear();
	}
	std::cout << "- step 2/5 Creating new segments" << std::endl;
	
    // sorting vectors
	std::sort(rd.begin(),rd.end(), compareData); 
	std::sort(sv.begin(),sv.end(), compareData); 
	
	crunch(rd, sv, segment, segmentXY);
	rd.empty();
	sv.empty();
	
	std::cout << "- step 3/5 Normalising ReadDepth and SVDetect values " << std::endl;
	normalize(segment, cnvNator);
	normalize_XY(segmentXY, cnvNatorXY);
	
	return EXIT_SUCCESS;
}
