#include "../combine/combine_XY.h"
#include "../compress/compress_XY.h"

void combine_XY(std::vector<NewSegmentFrame> const &cnv, std::vector <cnvNatorFrame> &cnvNatorXY){
	std::map <float, int> freq;
	std::vector <cnvFrame> cnv_combined;
    
    bool found = false;
	std::stringstream nr_decimals; // used to modify the number of decimals, 
	
    float cnv_value; // from 4 decimals to 1 decimal
	nr_decimals.precision(1);
	
	for(int i = 0; i < cnv.size(); ++i){
		auto search = freq.find(cnv[i].rdValue);
		if (search == freq.end()){
			freq.insert(std::pair<float, int>(cnv[i].rdValue, 0));
		}
		else{
			freq[cnv[i].rdValue] = freq[cnv[i].rdValue] + 1; 
		}
	}
	int local_minimum_freq = std::numeric_limits<int>::max();
	//float a_threshold = 0; // amplifications threshold
	//float d_threshold = 0; // deletions threshold
	
	for(const auto& i : freq){
		if (i.first >= 1 && i.first <= 1.35){
			if (i.second < local_minimum_freq){
				local_minimum_freq = i.second;
				a_threshold_XY = i.first;
			}
		}
	}
	
	d_threshold_XY = 1 - (a_threshold_XY - 1);

	for (int i = 0; i < cnv.size(); ++i){
    coverageMetaCNV_XY = ( (coverageMetaCNV_XY *1.0 ) + ( (cnv[i]._end * 1.0) - (cnv[i]._start *1.0 ) ) );
    coverageMetaCNV = ( (coverageMetaCNV *1.0 ) + ( (cnv[i]._end * 1.0) - (cnv[i]._start *1.0 ) ) );
    
		if (cnv[i].rdValue == NULL && cnv[i].svValue == NULL){
			cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 0.0, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(0.0-2.0,2), "no data from callers");
		}
		else{
			if (cnv[i].rdValue == NULL && cnv[i].svValue != NULL){
				if (cnv[i].svValue < a_threshold_XY){
					cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 0.0, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(0.0-cnv[i].svValue,2), "no data from callers");
				}
				else{
					if (cnv[i].svValue >= a_threshold_XY){
						nr_decimals << std::fixed << cnv[i].svValue;
						nr_decimals >> cnv_value;
						cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv_value * cnv[i].svEfactor, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(0.0-cnv[i].svValue,2), "SV (RD is Null)");
						nr_decimals.clear();
					}
				}
			}
			else{
				if (cnv[i].rdValue != NULL && cnv[i].svValue != NULL){
					if (cnv[i].rdValue >= a_threshold_XY && cnv[i].svValue <= a_threshold_XY){
						if (!cnvNatorXY.empty()){
							for (int k = 0; k < cnvNatorXY.size(); ++k){
								
								found = false;
								if (cnv[i].chr == cnvNatorXY[k].chr && ( (cnv[i]._start >= cnvNatorXY[k]._start && cnv[i]._start < cnvNatorXY[k]._end) || (cnv[i]._end >= cnvNatorXY[k]._start && cnv[i]._end < cnvNatorXY[k]._end) ) )  {
								
									if (cnvNatorXY[k].mutation == "deletion"){
										cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, std::min(cnv[i].rdValue, cnv[i].svValue), cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, cnvNatorXY[k].mutation, pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 1 (RD:AMP, SV:DEL) judge:CNVnator:DEL");
										found = true;
										break;
									}
									else{
										cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, std::max(cnv[i].rdValue, cnv[i].svValue), cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, cnvNatorXY[k].mutation, pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 1 (RD:AMP, SV:DEL) judge:CNVnator:AMP");
										found = true;
										break;
									}
								}
							}
							if (found == false){
								// not sure if this change (1.0 to cnv.RD) is correct:
								cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv[i].rdValue, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "no data", pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 1 (RD:AMP, SV:DEL)");
                                //cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 1.0, cnv[i].rdValue, cnv[i].svValue, "Conflict1 (RD:AMP, SV:DEL)");
							}
						}
						else{
                            // not sure if this change (1.0 to cnv.RD) is correct:
							cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv[i].rdValue, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "no data", pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 1 (RD:AMP, SV:DEL)");
                            //cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 1.0, cnv[i].rdValue, cnv[i].svValue, "Conflict1 (RD:AMP, SV:DEL)");
						}
					}
					else{
						if (cnv[i].rdValue < a_threshold_XY && cnv[i].svValue >= a_threshold_XY){
							if (!cnvNatorXY.empty()){
								for (int k = 0; k < cnvNatorXY.size(); ++k){
									
									found = false;
									if (cnv[i].chr == cnvNatorXY[k].chr && ( (cnv[i]._start >= cnvNatorXY[k]._start && cnv[i]._start < cnvNatorXY[k]._end) || (cnv[i]._end >= cnvNatorXY[k]._start && cnv[i]._end < cnvNatorXY[k]._end) ) ) {
								
										if (cnvNatorXY[k].mutation == "deletion"){
											cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, std::min(cnv[i].rdValue, cnv[i].svValue), cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, cnvNatorXY[k].mutation, pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 2 (RD:DEL, SV:AMP) judge:CNVnator:DEL");
											found = true;
											break;
										}
										else{
											cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, std::max(cnv[i].rdValue, cnv[i].svValue), cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, cnvNatorXY[k].mutation, pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 2 (RD:DEL, SV:AMP) judge:CNVnator:AMP");
											found = true;
											break;
										}
									}
								}
								if (found == false){
									// not sure if this change (1.0 to cnv.RD) is correct:
									cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv[i].rdValue, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "no data", pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 2 (RD:DEL, SV:AMP)");
                                    //cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 1.0, cnv[i].rdValue, cnv[i].svValue, "Conflict 2 (RD:DEL, SV:AMP)");
									
								}
							}
							else{
                                // not sure if this change (1.0 to cnv.RD) is correct:
								cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv[i].rdValue, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "no data", pow(cnv[i].rdValue-cnv[i].svValue,2), "Conflict 2 (RD:DEL, SV:AMP)");
                                //cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 1.0, cnv[i].rdValue, cnv[i].svValue, "Conflict 2 (RD:DEL, SV:AMP)");
							}
						}
						else{
							if (cnv[i].rdValue < a_threshold_XY && cnv[i].svValue < a_threshold_XY){
								nr_decimals << std::fixed << cnv[i].rdValue;
								nr_decimals >> cnv_value;
								cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv_value, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor,  "not assigned", pow(cnv[i].rdValue-cnv[i].svValue,2), "RD");
								nr_decimals.clear();
							}
							else{
								if (cnv[i].rdValue >= d_threshold_XY && cnv[i].svValue >= a_threshold_XY){
									nr_decimals << std::fixed << cnv[i].svValue;
									nr_decimals >> cnv_value;
									cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv_value * cnv[i].svEfactor, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(cnv[i].rdValue-cnv[i].svValue,2),  "SV");
									nr_decimals.clear();
								}
							}
						}
					}
				}
				else{
					if (cnv[i].rdValue != NULL && cnv[i].svValue == NULL){
						nr_decimals << std::fixed << cnv[i].rdValue;
						nr_decimals >> cnv_value;
						cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, cnv_value, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(cnv[i].rdValue-2.0, 2), "RD (SV is Null)");
						nr_decimals.clear();
					}
					else{
						cnv_combined.emplace_back(cnv[i].chr, cnv[i]._start, cnv[i]._end, 1.0, cnv[i].rdValue, cnv[i].svValue, cnv[i].rdBias, cnv[i].svEfactor, "not assigned", pow(0.0 - 2.0, 2), "unknown Error");
					}
				}
			}
		}
	}
	for (int i = 0; i < cnv_combined.size(); ++i){
		/* // I think we should skip this since there is no cn=ploidy anymore
		if ( (cnv_combined[i].comment == "Conflict 1 (RD:AMP, SV:DEL)" || cnv_combined[i].comment == "Conflict 2 (RD:DEL, SV:AMP)" )  && 
            ( (cnv_combined[i-1].comment == "RD" && cnv_combined[i+1].comment == "RD") || (cnv_combined[i-1].comment == "SV" && cnv_combined[i+1].comment == "SV") ) ) {
			cnv_combined[i].value = cnv_combined[i-1].value;
			cnv_combined[i].comment = "Removed(Conflict 1) - " + cnv_combined[i-1].comment;
		}*/
	}
	compress_XY(cnv_combined);
}