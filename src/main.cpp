/*************************************************************
 * MetaCNV v 1.4                                                  *
 * Stefanie Friedrich, Remus Barbulescu                          *  
 * stefanie.friedrich@scilifelab.se                                  *
 * Sonnhammer Group, Scilifelab, Stockholm, Sweden        *
 * January 2020                                                 *
 *************************************************************/
#include "../src/new_segments/new_segments.h"
#include "../src/new_segments/new_segmentsF.h"
#include "../src/cnvnator/cnvnator.h"
#include "../src/cnvnator/cnvnator_female.h"
#include "../src/Interface/mcnvgui.h"

std::string gender = "";
std::string match = "";
std::string matchGenes = "";
std::string matchExons = "";
std::string match_file_in ="";
std::string match_file_out ="";
std::string filename = "";
std::string filenameXY = "";
std::string readDepth_filepath = "";
std::string svdetect_filepath = "";
std::string cnvNator_option = "";
std::string cnvNator_filepath = "";

float coverageRD = 0; 
float coverageSV  = 0;
float coverageCNVnator = 0;
float coverageMetaCNV = 0;
float coverageMetaCNV_XY = 0; 

float a_threshold = 0; // amplifications threshold
float d_threshold = 0; // deletions threshold
float a_threshold_XY = 0; // amplifications threshold for allosomes
float d_threshold_XY = 0; // deletions threshold for allosomes

int main(int argc, char *argv[]){
	std::cout << "\nMetaCNV version 1.4 (January 2020) \n " << std::endl;
    std::cout << "\nReference:  " << std::endl;
    std::cout << "\nSee https://bitbucket.org/sonnhammergroup/metacnv/src/master/ or" << std::endl;
    std::cout << "\n*** publication*** for more info." << std::endl;
    std::cout << "\nCopyright (c) " << std::endl;
    std::cout << "\n---------------------------------------------------------------------" << std::endl;
    
	std::vector <cnvNatorFrame> cnvNator;
	std::vector <cnvNatorFrame> cnvNatorXY;
	gtk_init (&argc, &argv);
	//mcnvGUI();
    
    if (argc == 2 && ( strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0 )  ) {
        std::cout << "\n---------------------------------------------------------------------" << std::endl;
        std::cout << "\nRunning MetaCNV without GUI: ./MetaCNV -parameters \n " << std::endl;
        std::cout << "\nUsage: ./MetaCNV [-g gender] [-s SVDetect file path] [-r ReadDepth file path] [-c CNVnator file path] [-o outfile name] [-G match to reference genome / gene level] [-E match to reference Genome/ exon level] \n " << std::endl;
        std::cout << "\n Parameters:  \n " << std::endl;
        std::cout << " -g \tgender [Female,Male] " << std::endl;
        
        std::cout << " -s \tpath to SVDetect file " << std::endl;
        std::cout << " -r \tpath to ReadDepth file " << std::endl;
        std::cout << " -c \tpath to CNVnator file  *optional* " << std::endl;
        
        std::cout << " -o \toutfile name; file written to MetaCNV/Output/  " << std::endl;
      
        std::cout << " -G \t[Yes, No]  *optional* " << std::endl;
        std::cout << " -E \t[Yes, No]  *optional* \n\n" << std::endl;
        return 0;
    }
    
    if (argc > 2 )  {  //&& ( strcmp(argv[1], "--nogui") == 0 ) 
        int options = 0;
        ::cnvNator_option = "No";
        ::matchGenes = "No";
        ::matchExons = "No";
        while ((options = getopt (argc, argv, "g:s:r:c:o:G:E:")) != -1) {
            switch (options) {
                case 'g' :
                    ::gender = optarg;
                    break;
                case 's' :
                    ::svdetect_filepath = optarg;
                    break;
                case 'r' :
                    ::readDepth_filepath = optarg;
                    break;
                case 'c' : 
                    ::cnvNator_filepath = optarg;
                    ::cnvNator_option = "Yes";
                    break;
                case 'o' : 
                    ::filename = optarg;
                    ::filename = ::filename + ".dat"; 
                    ::filenameXY = ::filename;
                    break;
                case 'G' : 
                    ::matchGenes = optarg;
                    break;
                case 'E' :
                    ::matchExons = optarg;
                    break;
                case '?': 
                    if (optopt == 'g' || optopt == 's' || optopt == 'r' || optopt == 'o')
                        fprintf (stderr, "\n... Missing argument ....\n", optopt); 
                default: /* '?' */
                    fprintf(stderr, "\n\nUsage: %s [ -g gender] [ -s SVDetect file path] [ -r ReadDepth file path] [ -c CNVnator file path] [ -o outfile name] [ -G match to reference genome / gene level] [ -E match to reference Genome/ exon level] \n", argv[0]);
                    exit(EXIT_FAILURE);
            }
        }
        std::cout << "\n---------------------------------------------------------------------" << std::endl;
        std::cout << "\nSegments will be written to  MetaCNV/Output/MetaCNV_segments_" << ::filename << std::endl;
        std::cout << "Parameters will be written to  MetaCNV/Output/MetaCNV_parameters_" << ::filename << std::endl;
    }
  
     if (argc == 1  ) {
            mcnvGUI();
            gtk_main();
     }
     
    else {
        printf("\n");
        //return 0;
    }
     
		clock_t start_t = clock();
    
 	// Checking the gender and also if cnvNator is included in the input files
	if (::gender == "Male") { 
		if (::cnvNator_option == "Yes") {
			cnv_nator(cnvNator, cnvNatorXY);
		}
		if (newSegments(cnvNator, cnvNatorXY) == EXIT_FAILURE) {
			return EXIT_FAILURE;
		}
	}

    else {
		if (::cnvNator_option == "Yes") {
			cnv_nator_female(cnvNator);
		}
		if (newSegmentsF(cnvNator) == EXIT_FAILURE){
			return EXIT_FAILURE;
		}
	}
    
 	clock_t stop_t = clock();
	std::cout << "\nExecution time: " << (stop_t - start_t)/double(CLOCKS_PER_SEC) << " seconds\n" << std::endl;
    
	return 0;
}
