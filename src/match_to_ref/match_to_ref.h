#ifndef _MATCH_TO_REF_H_
#define _MATCH_TO_REF_H_

#include "../libraries.h"
#include "../structures.h"

extern void match_to_ref(std::string filepath, std::string match_file_in, std::string match_file_out);

#endif