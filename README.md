MetaCNV is a copy number caller that infers reliable copy numbers for human genomes with a consensus approach. MetaCNV specializes in low coverage data, but also performs well on normal and high coverage data. MetaCNV integrates the results of multiple copy number callers and infers absolute and unbiased copy numbers for the entire genome. MetaCNV is based on a meta-model that bypasses the weaknesses of current calling models while combining the strengths of existing approaches. Currently, MetaCNV is based on ReadDepth, SVDetect, and CNVnator.     

#
#

[TOC]

# Installation
Following git clone MetaCNV, the permission can be changed with:

```
    cd bin/
    chmod 755 MetaCNV_v1.4
```

# Usage

The executable is in the MetaCNV/bin folder. The tool is for Linux only. MetaCNV runs with both a graphical user interface and via the command line.

To run MetaCNV through the command line, the parameters explained below need to be set:

```
    cd bin/
    ./MetaCNV_v1.4 [-g gender] [-s SVDetect file] [-r ReadDepth file] [-c CNVnator file] [-o outfile name] [-G match to reference genome/gene level] [-E match to reference genome/exon level]
```

Type for help:

```
   ./MetaCNV_v1.4 -h 
```

For running MetaCNV with the graphical user interface, run: 

```
    cd bin/
    ./MetaCNV_v1.4
```

If MetaCNV is installed on a server and the graphical user interface is preferred, X11 forwarding is required when logging into the server. 

### Set parameters
The information is needed to apply the correct ploidy for the allosomes.


### Set input caller files
- Choose ReadDepth file (HCC1187 example file in MetaCNV/Examples) *- Required*
- Choose SVDetect file  (HCC1187 example file in MetaCNV/Examples) *- Required*
- Choose CNVnator file  (HCC1187 example file in MetaCNV/Examples) *- Optional*

### Set name for the output file
The MetaCNV output is in the folder MetaCNV/Output.

### Mapping to a reference genome
MetaCNV output can be mapped to a reference genome on both gene level and exon level. An example of a reference genome (based on GRCh38.84) is in the [MetaCNV/Input/](Input/) folder.
Any other reference genome can be used if placed in this folder, named the same way, and structured (columns order) the same way.

Mapping on exon level might be interesting if many breakpoint within a gene occur and it is needed to know which exons or introns are affected. However, running mapping on exon level is time consuming.

The current process step is shown in the terminal. It takes approximately less than 3 min for MetaCNV to run without mapping to a reference genome.

# MetaCNV input files
Required are the results of input copy number callers. Currently are these ReadDepth and SVDetect; CNVnator is optional. For SVDetect and CNVnator a window size can be set; we recommend a 
window size of 400 bp and no overlap (for SVDetect). 

### ReadDepth (required)
When running ReadDepth for copy number calling, we followed the script as described on [github.com/chrisamiller/readDepth](https://github.com/chrisamiller/readDepth) and set the parameter as recommend (example parameter file and script to run are provided in [Examples/Input_caller_configurations_scripts](Examples/Input_caller_configurations_scripts/) for HCC1187).
However, for very low coverage data and if the calculated bin size is very large (e.g > 2 kbp) it might be worth to increase the fdr from 0.01 to 0.05 or even 0.1 which indirectly decreases the bin size.

### SVDetect (required)
For SVDetect we developed the novel idea of a Null alignment as matched pair sample to increase sensitivity for low coverage data. The Null alignment is an alignment file with zero coverage (see MetaCNV/Examples/Input_caller_configurations_scripts/SVDetect_simNullAlignment.bam). 
To run SVDetect we used the configuration anjd run script as shown in the SVDetect_configuration.config file for HCC1187 in [Examples/Input_caller_configurations_scripts/](Examples/Input_caller_configurations_scripts/).

### CNVnator (optional)
CNVnator was run according to the recommendation ([https://github.com/abyzovlab/CNVnator](https://github.com/abyzovlab/CNVnator)) and with a window of 400 bp (example script in [Examples/Input_caller_configurations_scripts/](Examples/Input_caller_configurations_scripts)).

# MetaCNV output files
MetaCNV produces a parameter file and a segment file with the name defined in the interface. If mapping to a reference genome was chosen, the resulting text file is saved in MetaCNV/Output.

The parameter file summarises the set parameters and contains in detail:
- Gender
- ReadDepth - infile name
- ReadDepth - bias & coverage
- SVDetect - infile name
- SVDetect - coverage
- CNVnator - infile name
- CNVnator - coverage
- MetaCNV - outfile name
- MetaCNV - coverage

The segment file contains the segments, inferred absolute copy numbers plus error score. 
The copy numbers of the input callers are summarised in the columns ReadDepth.cn.array, SVDetect.cn.array, CNVnator.type.array. 

If matching to a reference is chosen, MetaCNV outputs a text file containing gene names with gene start & end, corresponding MetaCNV segment (start & end) and MetaCNV copy number. If mapping on exon level is chosen, gene start and end are replaced by exon start and end.

# Error messages

   |  
------------------------- | ---------------------------   
(MetaCNV:87): Gtk-WARNING **: cannot open display:" | X11 forwarding must be activated when logging into the server (e.g. ssh -X user server). Alternatively, MetaCNV can be run locally on a desktop computer.
*bash: ./MetaCNV: Permission denied* | Permission can be changed with ``` chmod 755 MetaCNV ``` 
(mcnv:17055): Gtk-WARNING **: Theme parsing error: gtk.css: | In the file ~/.config/gtk-3.0/gtk.css, a semicolon is missing to mark the end of the css property
Bad lexical cast: source type value could not be interpreted as target - Aborted | In the ReadDepth output file, the segment start & end positions are sometimes written in a scientific style (e.g. 1e6) but have to be in integer format for MetaCNV


#     


##        


###        




*MetaCNV version 1.4*

*January 2020*
